# Script for getting a report on SNMP configuration for iDRAC

## Introduction
This script will return a report regarding the SNMP endpoints configuration for a group of Dell EMC servers using iDRAC out-of-band cards.

## Requirements
**Python3** must be installed on the system. Also **pip** must be installed to download some python external libraries.  
Once python 3 and pip are installed, to install the additional libraries you can use the **requirements.txt** file:
~~~
pip install -r requirements.txt
~~~
The above command will install **pyyaml** as well as **requests** python libraries.  
When that's done, you can start using the script!

## How to setup config.yml file
The script has been thought to work with different credentials levels as well as different server regions as well.  

### Credentials
Since iDRAC credentials might vary from region to region, or from server from server, there's a way to override credentials depending on where you place them. The script **ALWAYS** will use the most 
specific credentials provided. 
Just image the following config file: 
~~~
default_credentials:
  user: root
  pass: calvin
    
intentory:
  - group_name: EUR
    description: This group of servers belong to Europe
    credentials:
      user: root
      pass: mysecretpassword
    servers:
      - ip: 10.0.0.1 #This specific server will use its own credentials
        credentials:
          user: root
          pass: mysecretpassword
          
  - group_name: USA #Servers from this group will use group credentials
    description: US servers are placed here
    credentials:
      user: root
      pass: mysecretpassword
    servers:
      - ip: 10.0.1.1
      - ip: 10.0.1.2
      - ip: 10.0.1.3
      
  - group_name: ASIA #servers from this group will use default_credentials
    description: Servers place in the far east
    servers:
      - ip: 10.0.2.1
      - ip: 10.0.2.2
      - ip: 10.0.3.3
~~~
- First, we have the **default_credentials** dictionary, with user and pass. These are the credentials that the script will use in case no specific group or server credentials are set.
Take a look into *ASIA* group. No specific group or server credentials are set. That means the script will use **default_credentials** to try to authenticate against those servers.  
- Second, let's see the credentials at **group** level. If you check *USA* group, credentials at group level has been set. That means, for that group, default_credentials are overriden and
**group credentials** will be use instead. These one will be used across all server UNLESS specific server credentials are set.
- Third, see group *EUR*. The server with ip 10.0.0.1 has been set with **server credentials**. Also, the group *EUR* has been set with group credentials. Since server credentials are the most
specific, these ones will be used. Also take into account that if you add more servers into this group, and you don't sent server specific credentials, the group one will be used.

### Groups
As you can see from the above config.yml file, to create different groups is nothing but to use the list **intentory** and add elemts to it.  
Please check the following example with a list with three groups:
~~~
intentory:
  - group_name: Charmander
    description: Fire type servers
    servers:
      - ip: 10.0.0.1
  - group_name: Bulbasaur
    description: Plant type servers
    servers:
      - ip: 10.0.2.1
	  - ip: 10.0.2.2
	  - ip: 10.0.2.3
  - group_name: Squirtle
    description: Water type servers
    servers:
      - ip: 10.0.3.1
	  - ip: 10.0.3.2
	  - ip: 10.0.3.3
	  - ip: 10.0.3.4
	  - ip: 10.0.3.5
	  - ip: 10.0.3.6
~~~
It wasn't mentioned, but you can add credentials at all levels needed (default ones, group ones or server ones).  
Within each group, we can set the following:  
- **group_name**: Name to identify a group. It will be used in the final report to identify that group.
- **description**: Description that can be used to write additional info regarding the group. It is not being used by the script.
- **servers**: List of servers to query SNMP destinations from. Within this list, we use **ip** to identify the server. **PLEASE take into account that the ip value can be the IP itself or the fqdn of the server**

## Example output
Here you have an example output of the script. Please bare in mind that the script will notice if the server is not reachable or if credentials are wrong.  
In those two cases, rathen than the SNMP info being displayed, and unreachable or invalid credentials message will be displayed on the final report.  
Also the SNMP targets no configured won't be displayed in the final report (up to 8 in iDRAC). Only the enabled ones will be displayed.  
**result.yml**
~~~
ASIA:
  10.0.2.1: UNREACHABLE
  10.0.2.2: UNREACHABLE
  10.0.3.3: UNREACHABLE
EUR:
  10.113.0.46:
    SNMPAlert.1.Destination: 10.1.20.9
    SNMPAlert.1.State: Enabled
    SNMPAlert.4.Destination: 10.5.4.2
    SNMPAlert.4.State: Enabled
USA:
  10.0.1.1: UNREACHABLE
  10.0.1.2: UNREACHABLE
  10.0.1.3: UNREACHABLE
~~~
By default, the script will timeout if the server cannot be reach withing 5 seconds. That would mean that in the final report, an *UNREACHABLE* message will be displayed.  
Also, as you can see from the example, the server *10.113.0.46* has two SNMPAlert configured (1 and 4) and are the only ones being displayed. The disabled ones are ommited. 


Miguel Sama (miguel.sama@dell.com)



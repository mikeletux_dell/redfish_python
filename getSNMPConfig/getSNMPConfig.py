#########################################################
# Script to get servers SNMP config from Redfish API    #
# Author: Miguel Sama (miguel.sama@dell.com)            #
# Version: 0.1                                          #
#########################################################

#Imports
import yaml
import requests
import socket
#Disable SSL certificate check warnings
requests.packages.urllib3.disable_warnings() 

#Constants
CONFIG_FILENAME = "config.yml"
OUTPUT_FILENAME = "result.yml"
SOCKET_TIMEOUT = 5

#Functions
def readConfigFile(filename):
    with open("./" + filename) as f:
        try:
            data = yaml.safe_load(f)
            return data
        except yaml.YAMLError as exc:
            print(exc)
            
def getServerAuthenticationToken(server_addr, idrac_user, idrac_pass):
    #Implement error handling
    response = requests.post('https://' + server_addr + '/redfish/v1/Sessions', 
                                headers={'Content-Type': 'application/json'}, 
                                json={'UserName':idrac_user, 'Password':idrac_pass}, 
                                verify=False)
    #Raise an exception in case of invalid credentials
    response.raise_for_status()
    token = response.headers['X-Auth-Token']
    logoutUrl = response.headers['Location']
    return token, logoutUrl

def closeRedfishSession(server_addr, auth_token, logoutUrl):
    response = requests.delete('https://' + server_addr + logoutUrl,
                                headers={'Content-Type': 'application/json', 'X-Auth-Token':auth_token},
                                verify=False)
                                
def fromDictToYamlFile(data, filename):
    with open(filename, 'w') as f:
        yaml.dump(data,f, default_flow_style=False)
        
def getSNMPConfig(auth_token, server_addr):
    attributes_url = "/redfish/v1/Managers/iDRAC.Embedded.1/Attributes"
    response = requests.get('https://' + server_addr + attributes_url,
                            headers={'Content-Type': 'application/json', 'X-Auth-Token':auth_token},
                            verify=False)
    response_json = response.json()["Attributes"]
    smtpAlert = {}
    for i in range(1,9):
        if response_json["SNMPAlert." + str(i) + ".State"] == "Enabled":
            smtpAlert["SNMPAlert." + str(i) + ".State"] = "Enabled"
            smtpAlert["SNMPAlert." + str(i) + ".Destination"] = response_json["SNMPAlert." + str(i) + ".Destination"]
    return smtpAlert

def checkIfServerReachable(server_addr):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(SOCKET_TIMEOUT)
    try:
        s.connect((server_addr, 443))
        s.close()
        return True
    except socket.error as e:
        print("[ERROR] Server " + server_addr + " unreachable")
        return False

def main():
    final_info = {} #Create group list
    config = readConfigFile(CONFIG_FILENAME)
    for group in config["inventory"]:
        final_info[group["group_name"]] = {} #Create server dict
        for server in group['servers']:
            #Check server is reachable
            if checkIfServerReachable(server["ip"]):
                try:
                    #Get auth
                    if "credentials" in server:
                        print("Using server credentials")
                        token, logoutUrl = getServerAuthenticationToken(server["ip"], 
                                                                        server["credentials"]["user"], 
                                                                        server["credentials"]["pass"])
                    elif "credentials" in group:
                        print("Using group credentials")
                        token, logoutUrl = getServerAuthenticationToken(server["ip"], 
                                                                        group["credentials"]["user"], 
                                                                        group["credentials"]["pass"])
                    else:
                        print("Using default credentials")
                        token, logoutUrl = getServerAuthenticationToken(server["ip"], 
                                                                        config["default_credentials"]["user"], 
                                                                        config["default_credentials"]["pass"])
                    #Query the endpoint
                    smtpEndpoints = getSNMPConfig(token, server["ip"])
                    closeRedfishSession(server["ip"], token, logoutUrl)
            
                    final_info[group["group_name"]][server["ip"]] = smtpEndpoints
                #If credentials are not valid, we catch the exception rathen than stopping the script
                except requests.exceptions.HTTPError as e:
                    print("[ERROR] Server " + server["ip"] + " - Invalid credentials")
                    final_info[group["group_name"]][server["ip"]] = "Invalid credentials"
            #At this point the server is not reachable
            else:
                final_info[group["group_name"]][server["ip"]] = "UNREACHABLE"
                     
    fromDictToYamlFile(final_info, OUTPUT_FILENAME)
    #print(final_info)

main()
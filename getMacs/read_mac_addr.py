#########################################################
# Script to get servers MACs from Redfish API           #
# Author: Miguel Sama (miguel.sama@dell.com)            #
# Version: 20200117-01                                  #
#########################################################

#Imports
import yaml
import requests
import pyodata

#Constants
CONFIG_FILENAME = "config.yml"
OUTPUT_FILENAME = "result.yml"

#Functions
def readConfigFile(filename):
    with open("./" + filename) as f:
        try:
            data = yaml.safe_load(f)
            return data
        except yaml.YAMLError as exc:
            print(exc)

def getServerAuthenticationToken(server_addr, idrac_user, idrac_pass):
    #Implement error handling
    response = requests.post('https://' + server_addr + '/redfish/v1/Sessions', 
                                headers={'Content-Type': 'application/json'}, 
                                json={'UserName':idrac_user, 'Password':idrac_pass}, 
                                verify=False)
    #response_json = response.json()
    token = response.headers['X-Auth-Token']
    logoutUrl = response.headers['Location']
    return token, logoutUrl

def closeRedfishSession(server_addr, auth_token, logoutUrl):
    response = requests.delete('https://' + server_addr + logoutUrl,
                                headers={'Content-Type': 'application/json', 'X-Auth-Token':auth_token},
                                verify=False)

def getNetworkInterfaces(server_addr, auth_token):
    #Implement error handling
    interfaces = []
    response = requests.get('https://' + server_addr + '/redfish/v1/Systems/System.Embedded.1/NetworkInterfaces', 
                            headers={'Content-Type': 'application/json', 'X-Auth-Token':auth_token},
                            verify=False)
    response_json = response.json()
    for interface in response_json['Members']:
        interfaces.append(interface['@odata.id'])
    return interfaces

def getNetworkFunctionsFromInterface(server_addr, auth_token, interface_url):
    networkFunctions = []
    response = requests.get('https://' + server_addr + interface_url,
                            headers={'Content-Type': 'application/json', 'X-Auth-Token':auth_token},
                            verify=False)
    response_json = response.json()
    response = requests.get('https://' + server_addr + response_json['NetworkDeviceFunctions']['@odata.id'],
                            headers={'Content-Type': 'application/json', 'X-Auth-Token':auth_token},
                            verify=False)
    response_json = response.json() #LOSING REFERENCES
    for networkFunction in response_json['Members']:
        networkFunctions.append(networkFunction['@odata.id'])
    return networkFunctions

def getMACsPerNetworkFunction(server_addr, auth_token, networkFunction_url):
    mac_addrs = {}
    response = requests.get('https://' + server_addr + networkFunction_url,
                            headers={'Content-Type': 'application/json', 'X-Auth-Token':auth_token},
                            verify=False)
    response_json = response.json()
    mac_addrs['permanent_mac'] = response_json['Ethernet']['PermanentMACAddress']
    mac_addrs['virtual_mac'] = response_json['Ethernet']['MACAddress']
    return mac_addrs

def fromDictToYamlFile(data, filename):
    with open(filename, 'w') as f:
        yaml.dump(data,f, default_flow_style=False)

def main():
    final_info = {} #Create group list
    config = readConfigFile(CONFIG_FILENAME)
    for group in config['infrastructure']:
        final_info[group['group_name']] = {} #Create server dict
        for server in group['servers']:
            token, logoutUrl = getServerAuthenticationToken(server, config['credentials']['idrac_user'], config['credentials']['idrac_pass'])
            final_info[group['group_name']][server] = {} #Create interface dict
            interfaces = getNetworkInterfaces(server, token)
            for interface in interfaces:
                interface_name = interface.split('/')[-1] #Getting last item
                final_info[group['group_name']][server][interface_name] = {} #Create network function dict
                network_functions = getNetworkFunctionsFromInterface(server, token, interface)
                for network_function in network_functions:
                    macs = getMACsPerNetworkFunction(server, token, network_function)
                    network_function_name = network_function.split('/')[-1] #Getting last item
                    final_info[group['group_name']][server][interface_name][network_function_name] = macs
            closeRedfishSession(server, token, logoutUrl)
    fromDictToYamlFile(final_info, OUTPUT_FILENAME)

#Program entry point
main()

